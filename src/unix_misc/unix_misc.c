//#include <termios.h>
#include <stdio.h>
#include <fcntl.h>
//#include <sys/ioctl.h>
#include <unistd.h> 
#include <string.h>
#include <stdlib.h>
#include "../microrl/config.h"
#include "inc/tm4c123gh6pm.h"
#include "misc.h"

// Enable TIVA mode
#define TIVA

// String symbol extern definitions (defined in .bss section in linker script)
extern volatile char __Str1, __Str2, __Str3, __Str4;

// LED request types
enum RequestTypes
{
    LED_OFF = 0,
    LED_ON,
    LED_BLINK,
    LED_CMD_INVALID,
    LED_COLOR_RED,
    LED_COLOR_GREEN,
    LED_COLOR_BLUE,
    LED_COLOR_WHITE,
    LED_COLOR_INVALID
};

// Definition commands word
#define _CMD_HELP   "help"
#define _CMD_CLEAR  "clear"
#define _CMD_EXIT   "exit"
#define _CMD_LED    "led"
// Sub commands for led command
    #define _SCMD_ON    "on"
    #define _SCMD_OFF   "off"
    #define _SCMD_BLINK "blink"
    #define _SCMD_COLOR_BLUE "blue"
    #define _SCMD_COLOR_GREEN "green"
    #define _SCMD_COLOR_RED "red"
    #define _SCMD_COLOR_WHITE "white"
#define _CMD_READ   "read"
#define _CMD_WRITE  "write"
// Sub commands for read/write command
    #define _SCMD_MEMADDR "mem_address"
    #define _SCMD_STRSYM1 "str1"
    #define _SCMD_STRSYM2 "str2"
    #define _SCMD_STRSYM3 "str3"
    #define _SCMD_STRSYM4 "str4"

// Available  commands
char * keyworld [] = {_CMD_HELP, _CMD_CLEAR, _CMD_LED, _CMD_READ, _CMD_WRITE, _CMD_EXIT};
// Read/Write  subcommands
char * rw_keyworld [] = {_SCMD_MEMADDR, _SCMD_STRSYM1, _SCMD_STRSYM2, _SCMD_STRSYM3, _SCMD_STRSYM4};
// LED subcommands
char * led_keyworld [] = {_SCMD_ON, _SCMD_OFF, _SCMD_BLINK};
// LED colors
char * color_keyworld [] = {_SCMD_COLOR_RED, _SCMD_COLOR_BLUE, _SCMD_COLOR_GREEN, _SCMD_COLOR_WHITE};

#define _NUM_OF_CMD      sizeof(keyworld)/sizeof(char *)
#define _NUM_OF_LED_SCMD sizeof(led_keyworld)/sizeof(char *)
#define _NUM_OF_RW_SCMD  sizeof(rw_keyworld)/sizeof(char *)
#define _NUM_OF_LED_COLORS sizeof(color_keyworld)/sizeof(char *)

// Array for completion
char * compl_world [_NUM_OF_CMD + 1];

// 'name' var for store some string
#define _NAME_LEN 8
char name [_NAME_LEN];
int val;
uint32_t led_blink_addr;

// Function prototypes
static void initialize_uart();
static void initialize_LEDs();
void delayMs(int n);
int start_tivashell (void);
static void exec_led_command(int led_mode, int led_color);
int write_to_memory(void *address, uint32_t wrdata);
void print_memory_range(void *start, int num_of_bytes);
int parse_number(const char *arg, unsigned int *number);
void initialize_string_locations(volatile char *loc, char *string);
void enable_irq(void);
void disable_irq(void);

// Macro definitions for LEDs
#define LED_ADDR_BLUE     0x40025010   // Address of the blue LED
#define LED_ADDR_GREEN    0x40025020   // Address of the green LED
#define LED_ADDR_RED      0x40025008   // Address of the red LED
#define LED_ADDR_WHITE    0x40025038   // Address of all the LEDs
#define LED_ADDR_INVALID  0x40025000   // No LEDs enabled for writing

//*****************************************************************************

// UART initialization code
static void initialize_uart()
{
    // Provide clock to UART0
    SYSCTL_RCGCUART_R |= 0x01;

    // Provide clock to PORTA
    SYSCTL_RCGCGPIO_R |= 0x01;

    // Disable UART0
    UART0_CTL_R = 0;

    // Set UART0's integer part for 115200bps
    UART0_IBRD_R = 8;

    // Set UART0's fractional part for 115200bps
    UART0_FBRD_R = 44;

    // Configure the line control value for 1 stop bit, no FIFO,
    // no interrupt, no parity, and 8-bit data size
    UART0_LCRH_R = 0x60;

    // Set UART0 Enable, TXen & RXen bits
    UART0_CTL_R = 0x0301;

    // Enable ports PA0 & PA1 as Digital I/O
    GPIO_PORTA_DEN_R |= 0x03;

    // Select alternate functionality for PA0 & PA1
    GPIO_PORTA_AFSEL_R |= 0x03;

    // Select UART as alternate functionality for PA0 & PA1
    GPIO_PORTA_PCTL_R = 0x11;

    // Disable analog mode for port A[1:0]
    GPIO_PORTA_AMSEL_R &= 0xfffffffC;

    DEBUG("Finished configuring UART0\n");
}

//*****************************************************************************
// initialize a string symbol locations with contents of a string
void initialize_string_locations(volatile char *loc, char *string)
{
    for (; *(string) != '\0'; loc++, string++)
        *loc = *string;
}

//*****************************************************************************
// HW specific initialization code
void init (void){
    const char *greeting1 = "Namaste! This is string1.\r";
    const char *greeting2 = "Namaste! This is string2.\r";
    const char *greeting3 = "Namaste! This is string3.\r";
    const char *greeting4 = "Namaste! This is string4.\r";
#ifdef TIVA
    initialize_uart();
    initialize_LEDs();
    // Initialize the strings str1, str2, str3 & str4 to 0's
#if 1
    memcpy(&__Str1, greeting1, strlen(greeting1));
    memcpy(&__Str2, greeting2, strlen(greeting2));
    memcpy(&__Str3, greeting3, strlen(greeting3));
    memcpy(&__Str4, greeting4, strlen(greeting4));
#else    
    initialize_string_locations(&__Str1, greeting1);
    initialize_string_locations(&__Str2, greeting2);
    initialize_string_locations(&__Str3, greeting3);
    initialize_string_locations(&__Str4, greeting4);
#endif    
#endif
};

//*****************************************************************************
// Print callback for testing with Linux PC; needs to be modified for
// Tiva board
#define dprintf(...) printf("DEBUG (%s,%d): ", __FUNCTION__, __LINE__); printf(__VA_ARGS__); printf("\n");
#define eprintf(...) printf("ERROR (%s,%d): ", __FUNCTION__, __LINE__); printf(__VA_ARGS__); printf("\n");
#if 1
void print (const char * str)
{
#ifndef TIVA
    fprintf (stdout, "%s", str);
#else
    char *ptr = (char *)str;
    for (ptr = (char *)str; (ptr != NULL) && (*ptr != '\0'); ptr++)
    {
        // Wait till transmit FIFO becomes non-full
        while(UART0_FR_R & 0x0020);

        //DEBUG("Writing to TX FIFO\n");
        UART0_DR_R = *ptr;

        if (*ptr == '\r')
        {
            // Wait till transmit FIFO becomes non-full
            while(UART0_FR_R & 0x0020);

            // Transmit new line character also
            UART0_DR_R = '\n';
        }
    }
#endif
}
#endif

//*****************************************************************************
// Get char user pressed, no waiting Enter input
// This needs to be modified for the Tiva board
char get_char (void)
{
    int ch;
#ifndef TIVA
    struct termios oldt, newt;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
#else
    // Wait till receive FIFO becomes non-empty
    while(UART0_FR_R & 0x0010);

    // Read data from the UART0 data register
    ch = (0x00ff & UART0_DR_R);
#endif
    return ch;
}

//*****************************************************************************
// Print a help menu detailing the list of commands supported & their syntaxes
void print_help ()
{
    print ("Use TAB key for completion\n\rCommand:\n\r");
    print ("\tled {on | off | blink [blue/red/green/white]} - \n\r\t\tTurn on / turn off / blink an LED\n\r");
    print ("\tread { mem_address [hex address] [number of bytes to read] } - \n\r\t\tread and display <number_of_bytes> bytes from address \n\r");
    print ("\tread { [string_symbol_name] } \n\r\t\t- read and display NULL terminated string symbol name\n\r");
    print ("\twrite { mem_address [hex address] [number of bytes to write] [content in 32-bit space separated hex words] } \n\r\t\t- write hex words to the specified memory address\n\r");
    print ("\twrite { [string_symbol_name] \"content of string data\" } \n\r\t\t- write string data to specified string symbol's location \n\r");
    print ("\thelp  - this message\n\r");
    print ("\tclear - clear screen\n\r");
    print ("\texit - exit tivaterm\n\r");
}

//*****************************************************************************
// write a 4-byte word to the desired memory address

int write_to_memory(void *address, uint32_t wrdata)
{
    volatile uint32_t *destination = (uint32_t *) address;

    dprintf("Writing %x to address %p\n", (unsigned int)wrdata, destination);

    *destination = wrdata;

    return SUCCESS;
}

//*****************************************************************************
// print the contents of memory address from <start> address upto 
// <num_of_bytes> specified

#ifdef TIVA 
void print_memory_range(void *start, int num_of_bytes)
{
#define STRBUFSIZE 128
    size_t i;
    int counter = 0, strcnt = 0;
    static char strbuf[STRBUFSIZE];
    volatile uint8_t *masterblock = (uint8_t *) start;
    memset(strbuf, 0x0, STRBUFSIZE);
    for (i = 0; i < num_of_bytes; ++i) {
        if (i%8 == 0)
            strcnt += sprintf(strbuf, "\t%08x : ", (unsigned int)masterblock+counter);
        strcnt += sprintf(strbuf+strcnt, "%02x ", masterblock[i]);
        counter += 1;
        if ((i+1)%8 == 0) { 
                strcnt += sprintf(strbuf+strcnt, " | %04x \r", counter);
                strbuf[strcnt+1] = '\0';
                print(strbuf);
                memset(strbuf, 0x0, STRBUFSIZE);
                strcnt = 0;
        }
    }
    if ((num_of_bytes%8) != 0)
    {
        strbuf[strcnt+1] = '\0';
        print(strbuf);
        print("\r");
    }
}
#else
void print_memory_range(void *start, int num_of_bytes)
{
    size_t i;
    int counter = 0;

    volatile uint8_t *masterblock = (uint8_t *) start;
    printf("\t%p : ", (uint8_t *)masterblock + counter);
    for (i = 0; i < num_of_bytes; ++i) {
        printf("%02x ", masterblock[i]);
        if ((i+1) % 8 == 0) {
                counter+=8;
                printf(" | %04x \n\t%p : ", counter, (uint8_t *)masterblock + counter);
        }
    }
    printf("\n");
}
#endif


//*****************************************************************************
// parse a number, in decimal / hexadecimal format
int parse_number(const char *arg, unsigned int *number)
{
    int ret = SUCCESS;
    ret = sscanf(arg, "0x%x", number);
    if (ret == 0)
    {
        printf("Failed to scan as a hexadecimal number, will try decimal\n");
        ret = sscanf(arg, "%u", number);
        if (ret)
        {
            printf("Decimal version is %u\n", *number);
        }
        else
        {
            printf("Failed to parse a number!\n");
            return ERR_BAD_NUMBER;
        }
    }
    else
    {
        printf("Hex version is %x\n", *number);
    }
    return SUCCESS;
}

//*****************************************************************************
// execute callback for microrl library
// do what you want here, but don't write to argv!!! read only!!
int execute (int argc, const char * const * argv)
{
    int i = 0;
    int ret = SUCCESS;
    int length = 0;
    static unsigned int addr = 0;
    static unsigned int numbytes = 0;
    static uint32_t wrdata = 0;
    static char *strptr = NULL;
    char strbuf[100] = "";
    int led_mode = LED_CMD_INVALID;
    int led_color = LED_COLOR_INVALID;
#if 1
    // just iterate through argv word and compare it with your commands
    if (strcmp (argv[0], _CMD_HELP) == 0) {
        print ("TivaTerm v0.1\n\r");
        print_help ();        // print help
    }
    else if (strcmp (argv[0], _CMD_EXIT) == 0) {
        print ("Thanks for using TivaTerm, will say goodbye & come back again !\r");
        return 0;
    }
    else if (strcmp (argv[0], _CMD_CLEAR) == 0) {
        print ("\033[2J");    // ESC seq for clear entire screen
        print ("\033[H");     // ESC seq for move cursor at left-top corner
    }
    else if (strcmp (argv[0], _CMD_LED) == 0) {
        if (argc != 3)
        {
            print ("Sorry, invalid number of arguments! See help\n\r");
            return 0;
        }
        for (i = 1; i < argc; i++)
        {
            switch (i)
            {
                // 2nd argument should be ON/OFF/BLINK
                case 1: 
                    if (strcmp (argv[i], _SCMD_ON) == 0)
                    {
                        led_mode = LED_ON;
                    }
                    else if (strcmp (argv[i], _SCMD_OFF) == 0)
                    {
                        led_mode = LED_OFF;
                    }
                    else if (strcmp (argv[i], _SCMD_BLINK) == 0)
                    {
                        led_mode = LED_BLINK;
                    }
                    else
                    {
                        print("Sorry, invalid led sub-command; see help.\n\r");
                    }
                    break;

                // 3rd argument should be the color of the LED 
                case 2:
                    if (strcmp (argv[i], _SCMD_COLOR_BLUE) == 0)
                    {
                        led_color = LED_COLOR_BLUE;
                    }
                    else if (strcmp (argv[i], _SCMD_COLOR_RED) == 0)
                    {
                        led_color = LED_COLOR_RED;
                    }
                    else if (strcmp (argv[i], _SCMD_COLOR_GREEN) == 0)
                    {
                        led_color = LED_COLOR_GREEN;
                    }
                    else if (strcmp (argv[i], _SCMD_COLOR_WHITE) == 0)
                    {
                        led_color = LED_COLOR_WHITE;
                    }
                    else
                    {
                        print("Sorry, invalid led color; valid colors are : blue/red/green/white.\n\r");
                    }
                    break;

                default:
                    eprintf("Illegal argument index %d!\n\r", i);
                    break;
            }
        }
        //dprintf("led_mode is %d", led_mode);
        //dprintf("led_color is %d", led_color);
        if (led_mode != LED_CMD_INVALID && led_color != LED_COLOR_INVALID)
            exec_led_command(led_mode, led_color);
    }
    else if (strcmp (argv[0], _CMD_READ) == 0) {
        if (argc < 2)
        {
            print ("Sorry, invalid number of arguments! See help\n\r");
            return 0;
        }
        if (strcmp (argv[1], _SCMD_MEMADDR) == 0){
            if (argc != 4)
            {
                print ("Sorry, invalid number of arguments! See help for usage\n\r");
                return ERR_BAD_ARGS;
            }
            for (i = 2; i<4; i++)
            {
                switch (i)
                {
                    // Get the hexadecimal address of the memory location
                    case 2:
                        //sscanf(argv[i], "0x%x", &addr);
                        dprintf("addr is %s", argv[i]);
                        ret = parse_number(argv[i], &addr);
                        if (ret != SUCCESS)
                        {
                            print("Invalid address entered!\r");
                            return ret;
                        }
                        dprintf("Memory address is 0x%x", addr);
                        break;

                    case 3:
                        //sscanf(argv[i], "%u", &numbytes);
                        dprintf("numbytes is %s", argv[i]);
                        ret = parse_number(argv[i], &numbytes);
                        if (ret != SUCCESS)
                        {
                            print("Invalid number of bytes entered!\r");
                            return ret;
                        }
                        dprintf("Number of bytes to be read is %u", numbytes);
                        break;

                    default:
                        eprintf("Illegal argument index %d!\n\r", i);
                        return ERR_BAD_ARGS;
                        break;
                }
            }
            print_memory_range((void *)addr, numbytes); 
        }
        else if (strncmp (argv[1], "str", 3) == 0){
            switch(argv[1][3])
            {
                case '1':
                    strptr = (char *) &__Str1;
                    print("Reading str1...\r");
                    break;
                case '2':
                    strptr = (char *) &__Str2;
                    print("Reading str2...\r");
                    break;
                case '3':
                    strptr = (char *) &__Str3;
                    print("Reading str3...\r");
                    break;
                case '4':
                    strptr = (char *) &__Str4;
                    print("Reading str4...\r");
                    break;
                default:
                    print("Invalid string name!\r");
                    return ERR_INVALID_STRING;
                    break;
            }
            print(strptr);
        }
    }
    else if (strcmp (argv[0], _CMD_WRITE) == 0) {
        if (argc < 2)
        {
            print ("Sorry, not enough arguments! See help\n\r");
            return 0;
        }
        if (strcmp (argv[1], _SCMD_MEMADDR) == 0){
            if (argc <= 3)
            {
                print ("Sorry, not enough arguments! See help for usage\n\r");
                return ERR_BAD_ARGS;
            }
            dprintf("addr is %s", argv[2]);
            ret = parse_number(argv[2], &addr);
            if (ret != SUCCESS)
            {
                print("Invalid address entered!\r");
                return ret;
            }
            dprintf("Memory address is 0x%x", addr);

            for (i = 3; i<argc; i++)
            {
                //sscanf(argv[i], "%u", &numbytes);
                dprintf("wrdata is %s", argv[i]);
                ret = parse_number(argv[i], (unsigned int *)&wrdata);
                if (ret != SUCCESS)
                {
                    print("Invalid data entered!\r");
                    return ret;
                }
                write_to_memory((void *)(addr+(i-3)*4), wrdata);
            }
        }
        else if (strncmp (argv[1], "str", 3) == 0){
            if (argc <= 2)
            {
                print("Insufficient number of arguments. See help\r");
                return ERR_BAD_ARGS;
            }
            switch(argv[1][3])
            {
                case '1':
                    strptr = (char *) &__Str1;
                    print("Writing str1...\r");
                    break;
                case '2':
                    strptr = (char *) &__Str2;
                    print("Writing str2...\r");
                    break;
                case '3':
                    strptr = (char *) &__Str3;
                    print("Writing str3...\r");
                    break;
                case '4':
                    strptr = (char *) &__Str4;
                    print("Writing str4...\r");
                    break;
                default:
                    print("Invalid string name!\r");
                    return ERR_INVALID_STRING;
                    break;
            }
            for (i = 2; i<argc; i++)
            {
                strcat(strbuf, argv[i]);
                length = strlen(strbuf);
                strbuf[length] = ' ';
                strbuf[length+1] = '\0';
            }
            length = strlen(strbuf);
            strbuf[length] = '\r';
            strbuf[length+1] = '\0';
            strbuf[length+2] = '\0';
            print(strbuf);
            memcpy(strptr, strbuf, length+2);
        }
    }
#if 0
        else if (strcmp (argv[i], _CMD_NAME) == 0) {
            if ((++i) < argc) { // if value preset
                if (strlen (argv[i]) < _NAME_LEN) {
                    strcpy (name, argv[i]);
                } else {
                    print ("name value too long!\n\r");
                }
            } else {
                print (name);
                print ("\n\r");
            }
        } else if (strcmp (argv[i], _CMD_VER) == 0) {
            if (++i < argc) {
                if (strcmp (argv[i], _SCMD_DEMO) == 0) {
                    print ("demo v 1.0\n\r");
                } else if (strcmp (argv[i], _SCMD_MRL) == 0) {
                    print ("microrl v 1.2\n\r");
                } else {
                    print ((char*)argv[i]);
                    print (" wrong argument, see help\n\r");
                }
            } else {
                print ("version needs 1 parametr, see help\n\r");
            }
        }  else if (strcmp (argv[i], _CMD_LIST) == 0) {
            print ("available command:\n");// print all command per line
            for (int i = 0; i < _NUM_OF_CMD; i++) {
                print ("\t");
                print (keyworld[i]);
                print ("\n\r");
            }
        }
#endif        
    else {
        print ("command: '");
        print (argv[i]);
        print ("' Not found.\n\r");
    }
#endif    
    return 0;
}

#ifdef _USE_COMPLETE
//*****************************************************************************
// completion callback for microrl library
char ** complet (int argc, const char * const * argv)
{
    int j = 0;

    compl_world [0] = NULL;

    // Handle all basic command names
    if (argc == 1) {
        // get last entered token
        char * bit = (char*)argv [argc-1];
        // iterate through our available token and match it
        for (int i = 0; i < _NUM_OF_CMD; i++) {
            // if token is matched (text is part of our token starting from 0 char)
            if (strstr(keyworld [i], bit) == keyworld [i]) {
                // add it to completion set
                compl_world [j++] = keyworld [i];
            }
        }
    }
    // Handle all LED specific sub-commands / options
    // LED mode
    else if (strcmp (argv[0], _CMD_LED)==0)
    {
        switch (argc)
        {
            case 2:
                // iterate through subcommand for LED command types (on/off/blink)
                for (int i = 0; i < _NUM_OF_LED_SCMD; i++) {
                    if (strstr (led_keyworld [i], argv [argc-1]) == led_keyworld [i]) {
                        compl_world [j++] = led_keyworld [i];
                    }
                }
                break;

            case 3:
                // iterate through subcommand for LED colors
                for (int i = 0; i < _NUM_OF_LED_COLORS; i++) {
                    if (strstr (color_keyworld [i], argv [argc-1]) == color_keyworld [i]) {
                        compl_world [j++] = color_keyworld [i];
                    }
                }
                break;

            default:
                break;
        }
    }
    // Handle all Memory specific sub-commands / options
    // LED mode
    else if ((strcmp (argv[0], _CMD_READ) == 0) || (strcmp(argv[0], _CMD_WRITE) == 0))
    {
        switch (argc)
        {
            case 2:
                // iterate through subcommand for READ command types (mem_address/string_symbol_name)
                for (int i = 0; i < _NUM_OF_RW_SCMD; i++) {
                    if (strstr (rw_keyworld [i], argv [argc-1]) == rw_keyworld [i]) {
                        compl_world [j++] = rw_keyworld [i];
                    }
                }
                break;

            default:
                break;
        }
    }
    else { // if there is no token in cmdline, just print all available token
        for (; j < _NUM_OF_CMD; j++) {
            compl_world[j] = keyworld [j];
        }
    }
    // note! last ptr in array always must be NULL!!!
    compl_world [j] = NULL;
    // return set of variants
    return compl_world;
}
#endif

//*****************************************************************************
void sigint (void)
{
    print ("^C catched!\n\r");
}

//*****************************************************************************

// Initialize PortF to control the LEDs
static void initialize_LEDs ()
{
    /* Configure Port F for accessing the LEDs */
    SYSCTL_RCGC2_R |= 0x00000020;
    GPIO_PORTF_DIR_R = 0x0E;
    GPIO_PORTF_DEN_R = 0x0E;

    /* Configure the SysTick timer for blinking the LEDs @ 1Hz */
    NVIC_ST_RELOAD_R = 16000000-1;  /* reload with number of clocks per second */
    NVIC_ST_CTRL_R = 7;             /* enable SysTick interrupt, use system clock */
 
    led_blink_addr = LED_ADDR_INVALID;

    enable_irq();                   /* global enable interrupt */
}

//*****************************************************************************
/* SysTick interrupt handler : simply toggles the desired LED */
void SysTick_Handler(void)
{
    *((volatile uint32_t *)led_blink_addr) ^= 0xff; /* toggle the desired LED */
}
 
//*****************************************************************************
/* global enable interrupts */
void enable_irq(void)
{
    __asm  ("    CPSIE  I\n");
}

//*****************************************************************************
/* global disable interrupts */
void disable_irq(void)
{
    __asm ("CPSID  I\n");
}

//*****************************************************************************
/* delay n milliseconds (16 MHz CPU clock) */
void delayMs(int n)
{
    int i, j;
    for(i = 0 ; i < n; i++)
        for(j = 0; j < 3180; j++) {}
}

//*****************************************************************************
/* LED color to Port address map */
static uint32_t led_color_map(int led_color)
{
    uint32_t addr = LED_ADDR_INVALID;
    switch (led_color)
    {
        case LED_COLOR_BLUE:
            addr = LED_ADDR_BLUE;
            break;

        case LED_COLOR_GREEN:
            addr = LED_ADDR_GREEN;
            break;

        case LED_COLOR_RED:
            addr = LED_ADDR_RED;
            break;

        case LED_COLOR_WHITE:
            addr = LED_ADDR_WHITE;
            break;

        case LED_COLOR_INVALID:
            addr = LED_ADDR_INVALID;
            break;

        default:
            break;
    }

    return addr;
}

//*****************************************************************************
/* Logic to decide whether to turn on / turn off / blink an LED */
static void exec_led_command(int led_mode, int led_color)
{
    uint32_t addr = LED_ADDR_INVALID;

    addr = led_color_map(led_color);

    switch (led_mode)
    {
        case LED_ON:
            disable_irq();
            *((volatile uint32_t *)LED_ADDR_WHITE) = 0;
            led_blink_addr = LED_ADDR_INVALID;
            *((volatile uint32_t *)addr) = 0xff;
            break;

        case LED_OFF:
            disable_irq();
            led_blink_addr = LED_ADDR_INVALID;
            *((volatile uint32_t *)addr) = 0;
            break;

        case LED_BLINK:
            disable_irq();
            *((volatile uint32_t *)LED_ADDR_WHITE) = 0;
            led_blink_addr = addr;
            enable_irq();
            break;

        default:
            break;
    }
}

//*****************************************************************************
