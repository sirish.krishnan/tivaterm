/*
 * main.c
 *
 *  Created on: Mar 8, 2019
 *      Author: sirishk
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "microrl/microrl.h"
#include "unix_misc/misc.h"
#include "inc/tm4c123gh6pm.h"
#include "microrl/config.h"

extern char __Str1, __Str2, __Str3, __Str4;

// create microrl object and pointer on it
microrl_t rl;
microrl_t * prl = &rl;

//*****************************************************************************
int start_tivashell (void/*int argc, char ** argv*/)
{
    DEBUG("Calling init() to perform HW specific initialization\n");
    init ();
    // call init with ptr to microrl instance and print callback
    DEBUG("Calling microrl_init()\n");
    microrl_init (prl, print);
    // set callback for execute
    DEBUG("Calling microrl_set_execute_callback()\n");
    microrl_set_execute_callback (prl, execute);
    DEBUG("Address of __Str1 is %p\n", &__Str1);
    DEBUG("Address of __Str2 is %p\n", &__Str2);
    DEBUG("Address of __Str3 is %p\n", &__Str3);
    DEBUG("Address of __Str4 is %p\n", &__Str4);

#ifdef _USE_COMPLETE
    // set callback for completion
    DEBUG("Calling microrl_set_complete_callback()\n");
    microrl_set_complete_callback (prl, complet);
#endif
    // set callback for Ctrl+C
    DEBUG("Calling microrl_set_sigint_callback()\n");
    microrl_set_sigint_callback (prl, sigint);

    print("Namaste, welcome to TivaTerm!\r");
    while (1) {
        // put received char from stdin to microrl lib
        microrl_insert_char (prl, get_char());
    }
    return 0;
}

int main()
{
    start_tivashell();
    return 0;
}
