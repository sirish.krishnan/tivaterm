# tivaterm

TivaTerm is a debug shell which works over UART for the Tiva Launchpad TM4C123GH6PM microcontroller developed by Texas Instruments.

## Credits

A big THANK YOU to the following persons :
- Mr. Eugene Samoylov for his "microrl" library, which was used & modified for this project.
  He has done a terrific job & the software design is simply brilliant.
  The original source is available at the following link :
  https://github.com/Helius/microrl
- Mr. Haresh Dagale for the knowledge, encouragement & inspiration to develop this software.
- Mr. J Shankarappa for demystifying the TM4C123GH6PM register set & providing excellent example code.

## Folders description

* **src** - Contains all source files organized by Code Composer Studio. One can import all the files, build & run as-is on the microcontroller.
* **docs** - Houses the design document & other useful documents. 

## Installation

- Create a project on Code Composer Studio & import all the files & folders inside the src/ directory
- Build & run !
